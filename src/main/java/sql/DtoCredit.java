package sql;

import javax.persistence.*;

@Entity
@Table(name = "creditInfo", uniqueConstraints = {@UniqueConstraint(columnNames = "id")})
public class DtoCredit {
    private Integer id;
    private String name;
    private Integer age;
    private Integer sum;
    private String result;

    public DtoCredit(Integer id, String name, Integer age, Integer sum, String result) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sum = sum;
        this.result = result;
    }

    public DtoCredit() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    @Column(name = "age", nullable = false)
    public Integer getAge(){
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Column(name = "sum", nullable = false)
    public Integer getSum(){
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    @Column(name = "result", nullable = false)
    public String getResult(){
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    @Override
    public boolean equals(Object o){
        if(this == o) {
            return true;
        }
        if(o == null || getClass() != o.getClass()){
            return false;
        }
        DtoCredit that = (DtoCredit) o;
        return (id.equals(that.id)) && name.equals(that.name) && age.equals(that.age) && sum.equals(that.sum) && result.equals(that.result);
    }

}

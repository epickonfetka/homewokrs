package sql;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Properties;

@RestController
public class SqlEndpoint {

    private static Session session;
    private static Properties properties = new Properties();

    static {
        properties.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/credit");
        properties.setProperty("hibernate.connection.user", "user");
        properties.setProperty("hibernate.connection.password", "user");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect");
        properties.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("show_sql", "true");

        SessionFactory sessionFactory = new Configuration()
                .addAnnotatedClass(DtoCredit.class)
                .addProperties(properties)
                .buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @GetMapping(path = "/creditdb/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getCredit(@PathVariable Integer id){
       return session.get(DtoCredit.class, id);
    }

    @PostMapping(path = "/creditdb", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object postCredit(@RequestBody DtoCredit dtoCredit){
        String result = resultCredit(dtoCredit.getName(),dtoCredit.getAge(),dtoCredit.getSum());
        dtoCredit.setResult(result);
        Transaction transaction = session.beginTransaction();
        session.save(dtoCredit);
        transaction.commit();
        return dtoCredit;
    }


    private String resultCredit(String name, Integer age, Integer sum){
        if( age>=18 && (!name.equals("Bob")) && (sum<age*100)){
            return "Одобрен";
        } else return "Не одобрен";
    }
}

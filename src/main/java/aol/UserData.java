package aol;

public class UserData {
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String month;
    private String day;
    private String year;
    private String phone;

    public UserData(String firstName, String lastName, String password, String email, String month, String day, String year, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.month = month;
        this.day = day;
        this.year = year;
        this.phone = phone;
    }

    public String getMonth() {
        return month;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getDay() {
        return day;
    }

    public String getYear() {
        return year;
    }

    public String getPhone() {
        return phone;
    }
}

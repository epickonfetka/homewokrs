package aol;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class AolRegisterPage {
    private SelenideElement firstNameInput = $x("//input[@id='usernamereg-firstName']");
    private SelenideElement lastNameInput = $x("//input[@id='usernamereg-lastName']");
    private SelenideElement emailInput = $x("//input[@id='usernamereg-yid']");
    private SelenideElement passwordInput = $x("//input[@id='usernamereg-password']");
    private SelenideElement countrySelect = $x("//select[@name='shortCountryCode']");
    private SelenideElement phoneInput = $x("//input[@id='usernamereg-phone']");
    private SelenideElement monthSelect = $x("//select[@id='usernamereg-month']");
    private SelenideElement dayInput = $x("//input[@id='usernamereg-day']");
    private SelenideElement yearInput = $x("//input[@id='usernamereg-year']");
    private SelenideElement submitButton = $x("//button[@id='reg-submit-button']");

    public AolRegisterPage() {
        Selenide.open("https://login.aol.com/account/create");
    }

    public PhoneConfirmPage fillData(UserData userData){
        firstNameInput.sendKeys(userData.getFirstName());
        lastNameInput.sendKeys(userData.getLastName());
        emailInput.sendKeys(userData.getEmail());
        passwordInput.sendKeys(userData.getPassword());
        countrySelect.selectOptionByValue("RU");
        phoneInput.sendKeys(userData.getPhone());
        monthSelect.selectOptionByValue(userData.getMonth());
        dayInput.sendKeys(userData.getDay());
        yearInput.sendKeys(userData.getYear());
        submitButton.click();
        return new PhoneConfirmPage();
    }
}

package aol;

import com.codeborne.selenide.SelenideElement;
import smsApi.PhoneData;
import smsApi.SmsApi;

import static com.codeborne.selenide.Selenide.$x;

public class PhoneConfirmPage {
    private SelenideElement sendCodeButton = $x("//button[@name='sendCode']");
    private SelenideElement smsInput = $x("//input[@id='verification-code-field']");
    private SelenideElement verifyButton = $x("//button[@id='verify-code-button']");

    public void approveSmsCode(PhoneData phoneData) throws InterruptedException {
        sendCodeButton.click();
        String smsCode = getCode(phoneData.getPhoneId());
        smsInput.sendKeys(smsCode);
        verifyButton.click();
    }

    private String getCode(String phoneId) throws InterruptedException {
        return new SmsApi().getSmsCode(phoneId);
    }
}

package app.credit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

public class MyJson {
    public String name;
    public int age;
    public int summa;
    public String id;

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSumma() {
        return summa;
    }

    public void setSumma(int summa) {
        this.summa = summa;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MyJson postMyJson() {
        MyJson temp = new MyJson();
        temp.name = name;
        temp.age = age;
        temp.summa=summa;
        temp.id = new Long(new Date().getTime()).toString();
        return temp;
    }

    public void save(String path, ObjectMapper mapper) throws IOException {
        mapper.writeValue(new File(path), this);
    }

    public static List<String> showAll(){
        String pathFile = "/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/";
        File dir = new File(pathFile); //path указывает на директорию
        List<File> lst = new ArrayList<>();
        List<String> nameOfFile = new ArrayList<>();
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                lst.add(file);
                nameOfFile.add(file.getName());
            }
        }
        return nameOfFile;
    }

    public static String showRealInfo() throws IOException {
        List<JSONObject> data = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        JSONObject jsonObject = new JSONObject();
        String pathFile = "/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/";
        List<String> paths = showAll();
        for(int i =0; i<paths.size(); i++){
            JSONObject tempClient = new JSONObject();
            ClientData client = mapper.readValue(new File(pathFile + paths.get(i)),ClientData.class);
            tempClient.put("name", client.name);
            tempClient.put("age", client.age);
            tempClient.put("sum", client.sum);
            tempClient.put("id", client.id);
            data.add(tempClient);
        }
        jsonObject.put("clients", data);

        return jsonObject.toString();
    }
}

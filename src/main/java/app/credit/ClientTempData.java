package app.credit;

import java.util.Date;

public class ClientTempData {
    public String name;
    public Integer age;
    public Integer sum;

    public ClientData toClient(){
        ClientData temp = new ClientData();
        temp.age = age;
        temp.name = name;
        temp.sum = sum;
        temp.id=new Date().getTime();
        return temp;
    }
}

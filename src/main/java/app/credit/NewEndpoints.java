package app.credit;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sql.DtoCredit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

@RestController
public class NewEndpoints {
    private String basePath = "/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources";
    private ObjectMapper mapper = new ObjectMapper();


    @Operation(description = "Получение информации о клиенте")
    @GetMapping(path = "/requestJson/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getRequestJson(@PathVariable(required = false) String id) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("error", "Пользователя с таким айди не существует");
        try{
            Path path = Paths.get(basePath + id);
            return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
        } catch (Exception e){
            return jsonObject.toString();
        }
    }

    @Operation(description = "Создание клиента")
    @PostMapping(path = "/requestJson", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MyJson createMyJson(@RequestBody MyJson temp) throws IOException {
        MyJson myJson = temp.postMyJson();
        myJson.save(basePath + myJson.id, mapper);
        return myJson;
    }

    @Operation(description = "Удаление о клиенте")
    @DeleteMapping(path = "/requestJson/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteMyJson(@PathVariable String id) throws IOException {
        String myJson = getRequestJson(id);
        Path path = Paths.get(basePath + id);
        Files.deleteIfExists(path);
        return myJson;
    }

    @Operation(description = "Удаление клиента через Get запрос")
    @GetMapping(path = "/requestJson/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteMyJsonGet(@PathVariable String id) throws IOException {
        String myJson = getRequestJson(id);
        Path path = Paths.get(basePath + id);
        Files.deleteIfExists(path);
        return myJson;
    }



    @Operation(description = "Вывод всех клиентов")
    @GetMapping (path = "/requestJson/all",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getAll(){
        List<String> lst= MyJson.showAll();
        return lst;
    }

    @GetMapping(path = "/requestJson/allInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getAllInfo() throws IOException {
      return MyJson.showRealInfo();
    }

    private static Session session;
    private static Properties properties = new Properties();

    static {
        try {
            properties.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:3306/postgres");
            properties.setProperty("hibernate.connection.user", "postgres");
            properties.setProperty("hibernate.connection.password", "user");
            properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL10Dialect");
            properties.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

            properties.setProperty("hibernate.hbm2ddl.auto", "update");
            properties.setProperty("show_sql", "true");

            SessionFactory sessionFactory = new Configuration()
                    .addAnnotatedClass(DtoCredit.class)
                    .addProperties(properties)
                    .buildSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getUser(Integer id){
        DtoCredit credit = session.get(DtoCredit.class, id);
        System.out.println(credit.getAge());
        System.out.println(credit.getName());
        System.out.println(credit.getSum());
        System.out.println(credit.getResult());
    }

    public void createUser(DtoCredit dtoCredit) throws JSONException {
        String result = resultCredit(dtoCredit.getName(),dtoCredit.getAge(),dtoCredit.getSum());
        dtoCredit.setResult(result);
        Transaction transaction = session.beginTransaction();
        session.save(dtoCredit);
        transaction.commit();
    }

    @GetMapping(path = "/creditdb/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DtoCredit getCredit(@PathVariable Integer id){
        DtoCredit credit = session.get(DtoCredit.class, id);
        return credit;
    }

    @PostMapping(path = "/creditdb", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object postCredit(@RequestBody DtoCredit dtoCredit) throws JSONException{
        String result = resultCredit(dtoCredit.getName(),dtoCredit.getAge(),dtoCredit.getSum());
        dtoCredit.setResult(result);
        Transaction transaction = session.beginTransaction();
        session.save(dtoCredit);
        transaction.commit();
        return dtoCredit;
    }

    @DeleteMapping(path = "/creditdb/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteCredit(@PathVariable Integer id){
        try{
            DtoCredit dtoCredit = getCredit(id);
            Transaction transaction = session.beginTransaction();
            session.delete(dtoCredit);
            transaction.commit();
            return "{\"message\":\"success\"}";
        } catch (Exception e){
            return "{\"message\":\"fail\"}";
        }
    }

    @PutMapping(path = "/creditdb/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object updateCredit(@RequestBody DtoCredit dtoCredit, @PathVariable Integer id){
        try {
            session.clear();
            dtoCredit.setId(id);
            String result = resultCredit(dtoCredit.getName(),dtoCredit.getAge(),dtoCredit.getSum());
            dtoCredit.setResult(result);
            Transaction transaction = session.beginTransaction();
            session.update(dtoCredit);
            transaction.commit();
            return dtoCredit;
        } catch (Exception e){
            return "{\"message\":\"user not exist\"}";
        }

    }


    private String resultCredit(String name, Integer age, Integer sum){
        if( age>=18 && (!name.equals("Bob")) && (sum<age*100)){
            return "Одобрен";
        } else return "Не одобрен";
    }

}

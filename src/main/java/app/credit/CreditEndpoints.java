package app.credit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RestController
public class CreditEndpoints {
    public static String URL_TO_JSONS = "/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources";
    private ObjectMapper mapper = new ObjectMapper();

    @PostMapping(path = "/clients", produces = MediaType.APPLICATION_JSON_VALUE,
                consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ClientData createClient(@RequestBody ClientTempData temp) throws IOException {
        ClientData clientData = temp.toClient();
        clientData.saveClient(URL_TO_JSONS + clientData.id, mapper);
        return clientData;
    }

    @DeleteMapping(path = "/clients/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object deleteClient(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources"+id);
        return Files.deleteIfExists(path);
    }

    @GetMapping(path = "/clientsInfo/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getClients(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources"+id);
        return Files.readAllLines(Paths.get(String.valueOf(path))).toString();
    }

    @GetMapping(path = "/getResult/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getResult(@PathVariable(required = false) String id) throws IOException {
        Path path = Paths.get("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources"+id);
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  resultCredit(name,age,sum);
    }

    private String resultCredit(String name, int age, int summ) {
        return (age >= 18) && (!name.equals("Bob")) && (summ < age * 100) ? "Кредит одобрен"
                : "Отказано в кредите";
    }



}

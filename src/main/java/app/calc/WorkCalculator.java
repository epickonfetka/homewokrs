package app.calc;

import java.util.Scanner;
import java.util.Stack;

public class WorkCalculator {

    public static void main(String[] args){
        System.out.println("введите выражение");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        System.out.println("Ответ: " + new WorkCalculator().decide(str));
    }

    public double decide(String expression) {
        String rpn = expressionToRPN(expression);
        return rpnToAnswer(rpn);
    }


    private String expressionToRPN(String expression) {
        String current = " "; //переменная важная локал
        Stack<Character> stack = new Stack<>(); //принимает символы
        int priority;
        for (int i = 0; i < expression.length(); i++) //теперь пройдемся по всему выражению по символьно
        {
            priority = getP(expression.charAt(i));

            if (priority == 0) current += expression.charAt(i);
            if (priority == 1) stack.push(expression.charAt(i)); //пушим такой символ

            if (priority > 1) {
                current += ' ';

                while (!stack.empty()) {
                    //пока наш стек не пустой, если приретет верхнего(peek) нашего стека больше или равен нашему приретету текущего символа нашего выражения то мы достаем из стека и кидаем в курент
                    if (getP(stack.peek()) >= priority) {
                        current += stack.pop();

                    } else break;
                }
                stack.push(expression.charAt(i));
            }

            if (priority == -1) {
                current += ' ';
                while (getP(stack.peek()) != 1) {
                    current += stack.pop();
                }
                stack.pop();
            }
        }
        while (!stack.empty()) {
            current += stack.pop();
        }

        return current; //этот метод обратной польской натации
    }

    private double rpnToAnswer(String rpn) {
        String operand;
        Stack<Double> stack = new Stack<>();//реализуем чтобы останавливаться когда пробел 2222 222 и т.д.

        for (int i = 0; i < rpn.length(); i++) {
            if (rpn.charAt(i) == ' ')
                continue;
            if (getP(rpn.charAt(i)) == 0) {
                operand = new String();
                while (rpn.charAt(i) != ' ' && getP(rpn.charAt(i)) == 0) {
                    operand += rpn.charAt(i++);
                    if (i == rpn.length())
                        break;
                }

                stack.push(Double.parseDouble(operand));
            }


            if (getP(rpn.charAt(i)) > 1) {
                double a = stack.pop(), b = stack.pop();
                if (rpn.charAt(i) == '+') stack.push(b + a);
                if (rpn.charAt(i) == '-') stack.push(b - a);
                if (rpn.charAt(i) == '*') stack.push(b * a);
                if (rpn.charAt(i) == '/') stack.push(b / a);
            }
        }
        return stack.pop(); //этот метод будет переводить обратную польскую натацию в ответ
    }

    private int getP(char token) //этот метод на вход принимает символ-ТОКЕН
    {
        if (token == '*' || token == '/')
            return 3; //верни 3
        else if (token == '+' || token == '-')
            return 2; //верни 2
        else if (token == '(')
            return 1;
        else if (token == ')')
            return -1;
        else return 0;
    }
}

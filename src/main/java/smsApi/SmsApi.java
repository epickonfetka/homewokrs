package smsApi;

import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

public class SmsApi {
    private final String URL = "https://smshub.org/stubs/handler_api.php";
    private final String API = "14734Ua039725cc348eefc219aa956d3a94d0b"; //свой ключ вставьте

    public String getAccountBalance(){
        String body = given().contentType(ContentType.HTML)
                .queryParam("api_key", API)
                .queryParam("action", "getBalance")
                .get(URL)
                .then().log().all().extract().body().htmlPath().getString("body");
        String[] data = body.split(":");
        String balance = data[1];
        return balance;
    }

    public PhoneData getPhoneForAol(){
        //ACCESS_NUMBER:234242:79123456789
        String body = given().contentType(ContentType.HTML)
                .queryParam("api_key", API)
                .queryParam("action", "getNumber")
                .queryParam("service", "pm")
                .queryParam("country", "0")
                .get(URL)
                .then().log().all().extract().body().htmlPath().getString("body");
        String[] data = body.split(":");
        PhoneData phoneData = new PhoneData(data[1], data[2]);
        return phoneData;
    }

    public String getSmsCode(String phoneId) throws InterruptedException {
        //STATUS_OK:CODE
        String smsCode;
        while (true){
            String body = given().contentType(ContentType.HTML)
                    .queryParam("api_key", API)
                    .queryParam("action", "getStatus")
                    .queryParam("id", phoneId)
                    .get(URL)
                    .then().extract().body().htmlPath().getString("body");
            if(body.contains("STATUS_OK")){
                String[] data = body.split(":");
                smsCode = data[1];
                break;
            }
            Thread.sleep(5000);
        }
        return smsCode;
    }
}

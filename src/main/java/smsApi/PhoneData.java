package smsApi;

public class PhoneData {
    private String phoneId;
    private String phoneNumber;

    public PhoneData(String phoneId, String phoneNumber) {
        this.phoneId = phoneId;
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}

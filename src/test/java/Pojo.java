import com.fasterxml.jackson.annotation.JsonProperty;

public class Pojo {
    @JsonProperty("1")
    private String _1;
    @JsonProperty("2")
    private String _2;
    @JsonProperty("3")
    private String _3;
    @JsonProperty("4")
    private String _4;

    public Pojo(String _1, String _2, String _3, String _4) {
        this._1 = _1;
        this._2 = _2;
        this._3 = _3;
        this._4 = _4;
    }

    public Pojo() {
    }

    public String get_1() {
        return _1;
    }

    public String get_2() {
        return _2;
    }

    public String get_3() {
        return _3;
    }

    public String get_4() {
        return _4;
    }
}

import app.credit.ClientData;
import app.credit.MyJson;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static app.credit.CreditEndpoints.URL_TO_JSONS;
import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.given;

public class CreditTest {
    @DataProvider
    private Object[][] users(){
        return new Object[][]{
                {"Vova", 40, 500},
                {"Sahsa", 50, 20},
                {"Elena", 15, 300},
                {"Oleg", 20, 1800},
        };
    }
    @Test
    public void createClient(){
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("name", "Test");
        bodyJson.put("age", 30);
        bodyJson.put("sum", 3000);

        Response response = given().contentType(ContentType.JSON).body(bodyJson.toString()).post("http://localhost:8080/clients")
                .then().log().all().extract().response();
        JsonPath jsonPath = response.jsonPath();
        Assert.assertNull(jsonPath.getJsonObject("id"));
    }

    @Test
    public void deleteClient(){
        given().delete("http://localhost:8080/clients").then().log().all()
                .assertThat().statusCode(200);
    }

    @Test
    public void checkAllClients(){
        final List<ClientData> clientData = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/requestJson/allInfo")
                .then().log().all()
                .extract().body().jsonPath().getList("clients", ClientData.class);
        int sizeActual = clientData.size();
        int sizeExpected = MyJson.showAll().size();
        Assert.assertEquals(sizeActual,sizeExpected);
    }

    @Test
    public void checkIdExist(){
        final List<ClientData> clientData = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/requestJson/allInfo")
                .then().log().all()
                .extract().body().jsonPath().getList("clients", ClientData.class);
        clientData.forEach(x->Assert.assertNotNull(x.id));

        for(int i = 0; i<clientData.size(); i++){
            Assert.assertNotNull(clientData.get(i).id);
        }
    }

    @Test
    public void negativeAge(){
        final List<ClientData> clientData = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/requestJson/allInfo")
                .then().log().all()
                .extract().body().jsonPath().getList("clients", ClientData.class);
        List<ClientData> let30 = clientData.stream().filter(x->x.age<40).collect(Collectors.toList());
        Assert.assertEquals(let30.size(),2);
    }

    @Test
    public void negativeGetUser(){
        String error = given().contentType(ContentType.JSON).get("http://localhost:8080/requestJson/10")
                .then().log().all().extract().jsonPath().getString("error");
        Assert.assertEquals(error, "Пользователя с таким айди не существует");
    }

    private String getResult() throws IOException {
        String url = URL_TO_JSONS+"123";
        Path path = Paths.get(URL_TO_JSONS + "123");
        int age = 0;
        int sum = 0;
        String name = "";
        String[] params = Files.readAllLines(Paths.get(String.valueOf(path))).toString().split(",");
        for(int i = 0; i < params.length; i++){
            if(params[i].contains("age")){
                String[] values1 = params[i].split(":");
                age = Integer.parseInt(values1[1]);
            }
            if(params[i].contains("sum")){
                String[] values2 = params[i].split(":");
                sum = Integer.parseInt(values2[1]);
            }
            if(params[i].contains("name")){
                String[] values3 = params[i].split(":");
                name = values3[1].replace("\"", "");
            }
        }
        return  (age >= 18) && !(name.equals("Bob")) && (sum<= age * 100) ? "Вам одобрен кредит" : "Вам не одобрен кредит";
    }

    @Test
    public void clientByIdMatchesShema(){
        String s = "(9+3)*3a";

        given().get("http://localhost:8080/requestJson/10").then().log().all()
                .assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("swagger.json"));
    }
}

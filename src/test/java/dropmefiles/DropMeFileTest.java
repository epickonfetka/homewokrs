package dropmefiles;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.codeborne.selenide.Selenide.$x;

public class DropMeFileTest {
    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.headless = false;
    }

    @Test
    public void check(){
        DropMainPage dropMainPage = new DropMainPage();
        dropMainPage.correctUpload();
    }

    @Test
    public void switchToWindow(){
        Selenide.open("https://tankionline.com/play/");
        SelenideElement googleAuth = $x("//form//img[2]");
        switchToAnotherWindowHard(googleAuth);
        SelenideElement email = $x("//input[@type='email']");
        email.sendKeys("testuser");
    }

    @Test
    public void switchToTab(){
        Selenide.open("https://yandex.ru");
        SelenideElement weatherWidget = $x("//div[@class='weather__icon weather__icon_ovc']");
        switchToAnotherWindowEasy(weatherWidget);
        SelenideElement town = $x("//h1");
        String townInfo = town.getText();
        System.out.println(townInfo);
        Assert.assertEquals(townInfo, "Микрорайон Центр, Пермь");
    }

    @Test
    public void jsClick(){
        Selenide.open("https://yandex.ru/pogoda/?via=hl");
        SelenideElement monthStats = $x("//a[@class='yandex-header__nav-link yandex-header__nav-link_active_no ']/span[1]");
        Selenide.executeJavaScript("arguments[0].click()",monthStats);
       // Selenide.executeJavaScript("document.querySelector(\"body > header > div > div.yandex-header__nav.yandex-header__item > a:nth-child(2)\").click()");
    }

    @Test
    public void harlemShakeFunJs() throws InterruptedException {
        String jsScrpit = "javascript: (function () {\n" +
                "    function c() {\n" +
                "        var e = document.createElement(\"link\");\n" +
                "        e.setAttribute(\"type\", \"text/css\");\n" +
                "        e.setAttribute(\"rel\", \"stylesheet\");\n" +
                "        e.setAttribute(\"href\", f);\n" +
                "        e.setAttribute(\"class\", l);\n" +
                "        document.body.appendChild(e)\n" +
                "    }\n" +
                "    function h() {\n" +
                "        var e = document.getElementsByClassName(l);\n" +
                "        for (var t = 0; t < e.length; t++) {\n" +
                "            document.body.removeChild(e[t])\n" +
                "        }\n" +
                "    }\n" +
                "    function p() {\n" +
                "        var e = document.createElement(\"div\");\n" +
                "        e.setAttribute(\"class\", a);\n" +
                "        document.body.appendChild(e);\n" +
                "        setTimeout(function () {\n" +
                "            document.body.removeChild(e)\n" +
                "        }, 100)\n" +
                "    }\n" +
                "    function d(e) {\n" +
                "        return {\n" +
                "            height: e.offsetHeight,\n" +
                "            width: e.offsetWidth\n" +
                "        }\n" +
                "    }\n" +
                "    function v(i) {\n" +
                "        var s = d(i);\n" +
                "        return s.height > e && s.height < n && s.width > t && s.width < r\n" +
                "    }\n" +
                "    function m(e) {\n" +
                "        var t = e;\n" +
                "        var n = 0;\n" +
                "        while ( !! t) {\n" +
                "            n += t.offsetTop;\n" +
                "            t = t.offsetParent\n" +
                "        }\n" +
                "        return n\n" +
                "    }\n" +
                "    function g() {\n" +
                "        var e = document.documentElement;\n" +
                "        if ( !! window.innerWidth) {\n" +
                "            return window.innerHeight\n" +
                "        } else if (e && !isNaN(e.clientHeight)) {\n" +
                "            return e.clientHeight\n" +
                "        }\n" +
                "        return 0\n" +
                "    }\n" +
                "    function y() {\n" +
                "        if (window.pageYOffset) {\n" +
                "            return window.pageYOffset\n" +
                "        }\n" +
                "        return Math.max(document.documentElement.scrollTop, document.body.scrollTop)\n" +
                "    }\n" +
                "    function E(e) {\n" +
                "        var t = m(e);\n" +
                "        return t >= w && t <= b + w\n" +
                "    }\n" +
                "    function S() {\n" +
                "        var e = document.createElement(\"audio\");\n" +
                "        e.setAttribute(\"class\", l);\n" +
                "        e.src = i;\n" +
                "        e.loop = false;\n" +
                "        e.addEventListener(\"canplay\", function () {\n" +
                "            setTimeout(function () {\n" +
                "                x(k)\n" +
                "            }, 500);\n" +
                "            setTimeout(function () {\n" +
                "                N();\n" +
                "                p();\n" +
                "                for (var e = 0; e < O.length; e++) {\n" +
                "                    T(O[e])\n" +
                "                }\n" +
                "            }, 15500)\n" +
                "        }, true);\n" +
                "        e.addEventListener(\"ended\", function () {\n" +
                "            N();\n" +
                "            h()\n" +
                "        }, true);\n" +
                "        e.innerHTML = \" <p>If you are reading this, it is because your browser does not support the audio element. We recommend that you get a new browser.</p> <p>\";\n" +
                "        document.body.appendChild(e);\n" +
                "        e.play()\n" +
                "    }\n" +
                "    function x(e) {\n" +
                "        e.className += \" \" + s + \" \" + o\n" +
                "    }\n" +
                "    function T(e) {\n" +
                "        e.className += \" \" + s + \" \" + u[Math.floor(Math.random() * u.length)]\n" +
                "    }\n" +
                "    function N() {\n" +
                "        var e = document.getElementsByClassName(s);\n" +
                "        var t = new RegExp(\"\\\\b\" + s + \"\\\\b\");\n" +
                "        for (var n = 0; n < e.length;) {\n" +
                "            e[n].className = e[n].className.replace(t, \"\")\n" +
                "        }\n" +
                "    }\n" +
                "    var e = 30;\n" +
                "    var t = 30;\n" +
                "    var n = 350;\n" +
                "    var r = 350;\n" +
                "    var i = \"https://s3.amazonaws.com/moovweb-marketing/playground/harlem-shake.mp3\";\n" +
                "    var s = \"mw-harlem_shake_me\";\n" +
                "    var o = \"im_first\";\n" +
                "    var u = [\"im_drunk\", \"im_baked\", \"im_trippin\", \"im_blown\"];\n" +
                "    var a = \"mw-strobe_light\";\n" +
                "    var f = \"https://s3.amazonaws.com/moovweb-marketing/playground/harlem-shake-style.css\";\n" +
                "    var l = \"mw_added_css\";\n" +
                "    var b = g();\n" +
                "    var w = y();\n" +
                "    var C = document.getElementsByTagName(\"*\");\n" +
                "    var k = null;\n" +
                "    for (var L = 0; L < C.length; L++) {\n" +
                "        var A = C[L];\n" +
                "        if (v(A)) {\n" +
                "            if (E(A)) {\n" +
                "                k = A;\n" +
                "                break\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "    if (A === null) {\n" +
                "        console.warn(\"Could not find a node of the right size. Please try a different page.\");\n" +
                "        return\n" +
                "    }\n" +
                "    c();\n" +
                "    S();\n" +
                "    var O = [];\n" +
                "    for (var L = 0; L < C.length; L++) {\n" +
                "        var A = C[L];\n" +
                "        if (v(A)) {\n" +
                "            O.push(A)\n" +
                "        }\n" +
                "    }\n" +
                "})()";
        Selenide.open("https://career.habr.com/");
        Selenide.executeJavaScript(jsScrpit);
        Thread.sleep(30000); //надо подождать и посмотреть что будет происходить на протяжении 30 секунд
    }

    private void switchToAnotherWindowHard(SelenideElement button){
        String oldHandle = WebDriverRunner.getWebDriver().getWindowHandle();
        button.click();
        Set<String> handles =  WebDriverRunner.getWebDriver().getWindowHandles();
        String newHandle = handles.stream().filter(x->!x.equals(oldHandle)).findAny().get();
        Selenide.switchTo().window(newHandle);
    }

    private void switchToAnotherWindowEasy(SelenideElement button){
        button.click();
        Selenide.switchTo().window(1);
    }


}

package dropmefiles;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.io.File;

import static com.codeborne.selenide.Selenide.$x;

public class DropMainPage {
    private final SelenideElement uploadInput = $x("//input[@type='file']");

    public DropMainPage(){
        Selenide.open("https://dropmefiles.com/");
    }

    public void correctUpload(){
        File file = new File("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/resources1644339274336");
        uploadInput.uploadFile(file);
    }

}

package sms;

import aol.AolRegisterPage;
import aol.UserData;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import smsApi.PhoneData;
import smsApi.SmsApi;


import static com.codeborne.selenide.Selenide.*;

public class SmsTest {

    @BeforeMethod
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        //options.addExtensions(new File("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/extension_1_3_1_0.crx"));
        //options.addArguments("user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 15_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.2 Mobile/15E148 Safari/604.1\n");
        Configuration.browserCapabilities = new DesiredCapabilities();
        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY,options);
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.headless = false;
    }

    private UserData createFakeUser(PhoneData phoneData){
        //paveselenid41z@aol.com
        String phone = phoneData.getPhoneNumber();
        phone = phone.substring(1);
        UserData userData = new UserData("Pavel", "Selenidov" ,
                "tesc234@!pop", "pa1velaaiddc","2","13","1990",phone);
        return userData;
    }

    @Test
    public void captcha() {
        Selenide.open("https://recaptcha-demo.appspot.com/recaptcha-v2-checkbox.php");
        Selenide.switchTo().frame(0);
        $("#recaptcha-anchor").click();
        int a = 0;
    }



    @Test
    public void checkBalance() throws InterruptedException {
        PhoneData phoneData = new SmsApi().getPhoneForAol();
        UserData userData = createFakeUser(phoneData);
        new AolRegisterPage().fillData(userData).approveSmsCode(phoneData);
    }
}

import app.calc.WorkCalculator;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CalcFuzzTest {
    @Test
    public void fuzz(){
        WorkCalculator workCalculator = new WorkCalculator();
        List<String> fuzzExpressions = FuzzTestBase.getListRandom();
        List<String> correctExpressions = new ArrayList<>();
        for(int i = 0; i<fuzzExpressions.size(); i++){
            String currentExpression = fuzzExpressions.get(i);
            try {
                double result = workCalculator.decide(currentExpression);
                correctExpressions.add(currentExpression + " является верным и ответ " + result);
            } catch (Exception e){

            }
        }
        FuzzTestBase.printList(correctExpressions);
        Assert.assertTrue(correctExpressions.size()>0);
    }
}

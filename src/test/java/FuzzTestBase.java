import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FuzzTestBase {
    private final static int MAX_SIZE = 10000;
    private final static int MIN = 3;
    private final static int MAX = 15;

    private static char[] charArray = {
            '8','9','(',')','+','-','*','/', 'a', 'z', 'v'
    };

    private static Random random = new Random();

    public static List<String> getListRandom(){
        List<String> expressions = new ArrayList<>(MAX_SIZE);
        for(int i = 0; i<MAX_SIZE; i++){
            expressions.add(getRandomString());
        }
        return expressions;
    }
    private static String getRandomString(){
        int length = random.nextInt(MAX - MIN + 1) + MIN;
        StringBuilder stringBuilder = new StringBuilder(length);
        for(int i =0; i<length; i++){
            char randomChar = charArray[random.nextInt(charArray.length)];
            stringBuilder.append(randomChar);
        }
        return stringBuilder.toString();
    }

    public static void printList(List<String> expression){
        System.out.println("Размер списка " + expression.size());
        expression.forEach(System.out::println);
    }

}

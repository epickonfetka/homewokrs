import app.ExponentLogic;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.testng.annotations.DataProvider;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class SpringTest {
    //1 response
    //2 body
    //3 pojo

    @org.testng.annotations.Test(dataProvider = "testValues")
    public void test(String number, String pow, String result){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/" + number)
                .then().log().body()
                .body(pow, equalTo(result));
    }

    @DataProvider
    public Object[][] testValues(){
        return new Object[][]{
                {"4", "1", "4"},
                {"4", "2", "16"},
                {"4", "3", "64"},
                {"4", "4", "256"},
                {"2", "1", "2"},
                {"2", "2", "4"},
                {"2", "3", "8"},
                {"2", "4", "16"}
        };
    }

    @Test
    public void response(){
        Response response = given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/4")
                .then().log().body()
                .extract().response();
        JsonPath jsonPath = response.jsonPath();
        String one = jsonPath.getString("1");
        String two = jsonPath.getString("2");
        String three = jsonPath.getString("3");
        String four = jsonPath.getString("4");
        Assertions.assertEquals("4",one);
        Assertions.assertEquals("16",two);
        Assertions.assertEquals("64",three);
        Assertions.assertEquals("256",four);
    }

    @Test
    public void body(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/4")
                .then().log().body()
                .body("1", equalTo("4"))
                .body("2", equalTo("16"))
                .body("3", equalTo("64"))
                .body("4", equalTo("256"));
    }

    @Test
    public void pojo(){
        Pojo pojo = given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/4")
                .then().log().body()
                .extract().body().jsonPath().getObject("",Pojo.class);
        Assertions.assertEquals("4",pojo.get_1());
        Assertions.assertEquals("16",pojo.get_2());
        Assertions.assertEquals("64",pojo.get_3());
        Assertions.assertEquals("256",pojo.get_4());
    }

    @Test
    public void empty(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/multi/")
                .then().log().body()
                .body("error", equalTo("Напишите цифру, например /multi/2"));
    }

    @Test
    public void error404(){
        given()
                .contentType(ContentType.JSON)
                .get("http://localhost:8080/test/")
                .then().log().body()
                .assertThat().statusCode(404);
    }
}

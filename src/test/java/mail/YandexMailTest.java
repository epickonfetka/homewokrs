package mail;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import mail.api.MessageReader;
import me.shivzee.exceptions.MessageFetchException;
import me.shivzee.util.Message;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

import static com.codeborne.selenide.Selenide.$x;

public class YandexMailTest {
    @BeforeMethod
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addExtensions(new File("/Users/o.pendrak/Documents/springBootWebApi/src/main/resources/modheader.crx"));
        //options.addArguments("user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 15_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.2 Mobile/15E148 Safari/604.1\n");
        Configuration.browserCapabilities = new DesiredCapabilities();
        Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY,options);
        Configuration.browser = "chrome";
        Configuration.driverManagerEnabled = true;
        Configuration.headless = false;
        addCookie();
    }

    private void addCookie(){
        Selenide.open("https://google.com");
        Selenide.open("chrome-extension://idgpnmonknjnojddfkpgkljpfnnfcklj/popup.html");
        $x("//input[@placeholder='Name']").sendKeys("Cookie");
        $x("//input[@placeholder='Value']").sendKeys("yandexuid=1013721434135608436; _ym_uid=166430415056928; is_gdpr=0; mda=0; is_gdpr_b=COaIGBD7TygC; my=YwA=; amcuid=22134034511658; gdpr=0; L=dV9CCQcBAkVvXwhdSHtKXURhDUhFAS4vNSgwMRcXQQ==.1645640991.14897.310950.147abeffbb4caf8d0063f8024b6daa58; yandex_login=ChangeIt; i=ZgSAWDQk4Wrpy1FULebT4wIffQwrvqNwpDUtPjXjasd34dHmQulalE/6LmXomPo0QRXnXUe54VOT7JprJw=; yandex_gid=50; yabs-frequency=/5/0000000000000000/Wc51ROO0001eHIEgTzt4pdkDLbD5_F___m00/; EIXtkCTlX=1; Session_id=3:163444460.5.0.1645640991856:r1_8sg:1d.1.2:1|230180060.0.2|3:248807.213451._UZ12dmVFB0JXuVWHsNJELM-G2o; sessionid2=3:1646123460.5.0.1645640991856:r1_8sg:1d.1.2:1|230180060.0.2|3:248807.213451._UZ12dmVFB0JXuVWHsNJELM-G2o; _ym_d=1646153481; yp=1648743458.ygu.1#1661921484.szm.2:1680x1050:1101x840#1646831379.spcs.l#1648743543.los.1#1648743543.losc.0#1961000991.udn.cDpwZW5vbGVncnVz#1646245797.dq.1#1648319442.csc.2#1646318072.mcl.y80scr#1646756343.mcv.0#1646756343.mct.null; yabs-sid=2396714821646407404; _yasc=cXeJ6tgrziZq3ok24YHAL+uIyCUCJHz64WKutAQfGrMQUhK6U0D3dbtOmGcER4HqchBmtA==; _ym_isad=1; instruction=1; _ym_visorc=w; yuidss=101372143112338436; ymex=1961768980.yrts.1643308980#1950868436.yrtsi.1635508436");
        $x("//datalist[@id='request-autocomplete']//following::h3").click();
    }

    @Test
    public void messageCountTest() throws Exception {
        YandexMailMainPage yandexMailMainPage = new YandexMailMainPage();
        yandexMailMainPage.openMobileMail();
        Assert.assertEquals(30, yandexMailMainPage.getMessagesCount());
        yandexMailMainPage.showAllTitles();
    }

    @Test
    public void checkEmailTest() throws MessageFetchException, InterruptedException {
        String subject = "Thread sleep3";
        String body = "Thread body sleep body3";
        MessageReader messageReader = new MessageReader();
        YandexMailMainPage yandexMailMainPage = new YandexMailMainPage();
        yandexMailMainPage.openMobileMail()
                .createMessage()
                .sendMessage("eqbejfvjdsjxjp@metalunits.com", subject, body);

        Thread.sleep(10000);

        Message lastMessage = messageReader.getLastMessage();
        Assert.assertEquals(subject,lastMessage.getSubject());
        Assert.assertEquals(body,lastMessage.getContent());
        Assert.assertEquals("penolegrus@yandex.ru",lastMessage.getSenderAddress());
    }
}

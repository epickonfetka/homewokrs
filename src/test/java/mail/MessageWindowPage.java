package mail;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class MessageWindowPage {
    private final SelenideElement sendTo = $x("//label[text()='Кому']//following::input[1]");
    private final SelenideElement sendToField = $x("//input[@name='to']");
    private final SelenideElement subjectField = $x("//input[@name='subj']");
    private final SelenideElement bodyField = $x("//textarea[@name='send']");
    private final SelenideElement submitBtn = $x("//input[@name='doit']");

    public void sendMessage(String email, String subject, String body){
        sendToField.sendKeys(email);
        subjectField.sendKeys(subject);
        bodyField.sendKeys(body);
        submitBtn.click();
    }

}

package mail;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class YandexMailMainPage {
    private final SelenideElement writeMessage = $x("//a[@aria-label='Написать']");
    private final ElementsCollection messageTitles = $$x("//span[@role='presentation']//span[@class='b-messages__subject']");

    public MessageWindowPage createMessage(){
        writeMessage.click();
        return new MessageWindowPage();
    }

    public YandexMailMainPage openMobileMail(){
        Selenide.open("https://mail.yandex.ru/lite/inbox");
        return this;
    }

    public YandexMailMainPage openPcMail(){
        Selenide.open("https://mail.yandex.ru/");
        return this;
    }

    public int getMessagesCount(){
        return messageTitles.size();
    }

    public void showAllTitles(){
        messageTitles.forEach(x-> System.out.println(x.getText()));
    }

    public void getOneElement() throws Exception {
        SelenideElement element = messageTitles.stream()
                .filter(x->x.getText().equals("Владимир"))
                .findFirst().orElseThrow(() -> new Exception("Текста такого нет, соре"));
        System.out.println(element.getText());
    }
}

package mail.api;

import me.shivzee.JMailTM;
import me.shivzee.callbacks.MessageFetchedCallback;
import me.shivzee.callbacks.MessageListener;
import me.shivzee.exceptions.MessageFetchException;
import me.shivzee.util.JMailBuilder;
import me.shivzee.util.Message;
import me.shivzee.util.Response;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.List;

public class MessageReader {
    private JMailTM mailer;

    private List<Message> getAllMessages() throws MessageFetchException {
        List<Message> messageList = new ArrayList<>();
            mailer.fetchMessages(2,new MessageFetchedCallback() {
                @Override
                public void onMessagesFetched(List<Message> list) {
                    messageList.addAll(list);
                }
                @Override
                public void onError(Response response) {

                }
            });
        return messageList;
    }

    public Message getLastMessage() throws MessageFetchException {
        return getAllMessages().get(0);
    }

    public MessageReader() {
        try {
            mailer = JMailBuilder.login("eqbejfvjdsjxjp@metalunits.com", "mMQXa;UE");
            mailer.init();
            System.out.println("Email : " + mailer.getSelf().getEmail());

            mailer.openMessageListener(new MessageListener() {
                @Override
                public void onMessageReceived(Message message) {
                    System.out.println("Message Has Attachments ?  : " + message.hasAttachments());
                    System.out.println("Message Content : " + message.getContent());
                    System.out.println("Message RawHTML : " + message.getRawHTML());
                    // To Mark Message As Read
                    message.markAsRead(status -> {
                        System.out.println("Message " + message.getId() + " Marked As Read");
                    });
                }

                @Override
                public void onError(String error) {
                    System.out.println("Some Error Occurred " + error);
                }
            });

        } catch (LoginException exception) {
            System.out.println("Exception Caught " + exception);
        }
    }
}


package sql;

import app.credit.NewEndpoints;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Properties;

import static io.restassured.RestAssured.given;

public class SqlTest {

    @Test
    public void update(){
        DtoCredit user = new DtoCredit(400,"Zamena", 24,3000,"Net");
        given().contentType(ContentType.JSON)
                .body(user)
                .put("http://localhost:8080/creditdb/2")
                .then().log().all();
    }

    @Test
    public void idTest(){
        DtoCredit user = new DtoCredit(400,"Aleksandr", 30,4000,"4");

        DtoCredit createdUser =  given().contentType(ContentType.JSON)
                .body(user)
                .post("http://localhost:8080/creditdb")
                .then().log().all().extract().as(DtoCredit.class);

        Assert.assertNotNull(createdUser.getId());
    }

    @Test
    public void createUser(){
        DtoCredit user = new DtoCredit(400,"Vladimir", 20,30,"lol");

        DtoCredit createdUser =  given().contentType(ContentType.JSON)
                .body(user)
                .post("http://localhost:8080/creditdb")
                .then().log().all().extract().as(DtoCredit.class);

        DtoCredit infoUser = given().contentType(ContentType.JSON)
                .get("http://localhost:8080/creditdb/" + createdUser.getId())
                .then().log().all().extract().as(DtoCredit.class);

        Assert.assertEquals(createdUser,infoUser);
    }

    @Test
    public void deleteUser(){
       Response response =  given().delete("http://localhost:8080/creditdb/2")
               .then().log().all().extract().response();
       String message = response.body().jsonPath().getString("message");
       Assert.assertEquals(message,"success");
    }

    @Test
    public void compare(){
        DtoCredit first = new DtoCredit(1,"Test", 30,3000,"Да");
        DtoCredit second = new DtoCredit(1,"Test", 30,3000,"Да");
        Assert.assertEquals(first,second);
    }
}
